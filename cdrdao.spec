%global bname rel_1_2_5
Summary:   Records audio or data CD-Rs in disk-at-once (DAO) mode
Name:      cdrdao
Version:   1.2.5
Release:   1
License:   GPLv2+
URL:       http://cdrdao.sourceforge.net/
Source0:   https://github.com/cdrdao/%{name}/archive/%{bname}/%{name}-%{version}.tar.gz

BuildRequires:  gcc-c++ gcc libsigc++20-devel libao-devel
BuildRequires:  libvorbis-devel >= 1.0 libmad-devel
BuildRequires:  lame-devel autoconf GConf2-devel

%description
Cdrdao records audio or data CD-Rs in disk-at-once (DAO) mode based
on a textual description of the CD contents (toc-file).

%package_help

%prep
%autosetup -n %{name}-%{bname} -p 1
if [ -e autogen.sh ]; then
    sed -i '/autoreconf/d' autogen.sh
    ./autogen.sh
fi

%build
autoreconf -v -f -i -I.
%configure --without-xdao --without-scglib --with-ogg-support --with-mp3-support --with-lame
%make_build


%install
%make_install
%delete_la


%files
%license COPYING
%{_bindir}/cdrdao
%{_bindir}/cue2toc
%{_bindir}/toc2cddb
%{_bindir}/toc2cue
%{_bindir}/toc2mp3
%{_datadir}/cdrdao

%files help
%doc AUTHORS README CREDITS ChangeLog
%{_mandir}/man1

%changelog
* Sun Apr 23 2023 xu_ping <707078654@qq.com> - 1.2.5-1
- Upgrade package to 1.2.5 version.

* Thu Nov 26 2020 maminjie <maminjie1@huawei.com> - 1.2.3-37
- switch Source0 to github and optimize patch file

* Thu Jan 16 2020 fengbing <fengbing7@huawei.com> - 1.2.3-36
- Type:N/A
- ID:N/A
- SUG:N/A
- DESC:modify patch in spec file

* Tue Nov 19 2019 mengxian <mengxian@huawei.com> - 1.2.3-35
- Package init
